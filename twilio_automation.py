from twilio.rest import TwilioRestClient
import gspread
import json
from oauth2client.client import SignedJwtAssertionCredentials
from datetime import datetime 
import time
from random import randint
import sys
from keys import *
import csv


json_key = json.load(open('twilio-gspread-a8460e66a981.json'))
scope = ['https://spreadsheets.google.com/feeds']

def parse_parameters():
	global recipient_numbers
	recipient_numbers = {}
	global sms_or_call
	sms_or_call = None

	for dict_pair in sys.argv:
		if dict_pair == sys.argv[0]:
			pass
		elif dict_pair == sys.argv[1]:
			if sys.argv[1] == "call":
				sms_or_call = "call"
			elif sys.argv[1] == "sms":
				sms_or_call = "sms"
		else:
			key, value = dict_pair.split("=")
			if key in recipient_numbers:
				recipient_numbers[key].append(value)
			else:
				recipient_numbers[key] = value

	print "Numbers Added to Script:" 
	print recipient_numbers

def oauth_login():
	print "Logging into Google"
	credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)
	gc = gspread.authorize(credentials)

	if sms_or_call == "call":
		sh = gc.open("Twilio_Call_Logs")
	elif sms_or_call == "sms":
		sh = gc.open("Twilio_SMS_Logs")

	todays_date = datetime.today().strftime("%m/%d/%Y")
	global worksheet
	worksheet = sh.add_worksheet(title=todays_date, rows="1000", cols="10")
	title_row = {
		'A1': 'Name of Recipient',
		'B1': 'Phone Number',
		'C1': 'Counter',
		'D1': 'SID',
		'E1': 'Datetime Sent'
	}

	for cell, title in title_row.items():
		worksheet.update_acell(cell, title)

def send_message(counter, hash_of_numbers):
	client = TwilioRestClient(twilio_account, twilio_access_token)

	for name, number in hash_of_numbers.items():
		current_date = datetime.today()
		if sms_or_call == "call":
			call = client.calls.create(to=number, from_=twilio_num, url="http://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient")
			message_info = [name, number, counter, call.sid, current_date]

			print "-"*40
			print "Making call to {} at ".format(name) + str(current_date)
			time.sleep(10)
			print "Hanging up"
			call = client.calls.update(call.sid, status="completed")
			
		elif sms_or_call == "sms":
			sms = client.messages.create(to=number, from_=twilio_num, body=counter)
			message_info = [name, number, counter, sms.sid, current_date]

			print "-"*40
			print "Sending message to {} at ".format(name) + str(current_date)


		add_to_csv(message_info)

def create_new_csv():
	global current_csv_file
	current_csv_file = 'messages_{}.csv'.format(datetime.today())

def add_to_csv(array_from_send_message):
	with open(current_csv_file.format(datetime.today()), 'a') as csvfile:
		blargh = csv.writer(csvfile)
		blargh.writerow(array_from_send_message)

def add_to_google_sheet():
	print "Logging Data onto Google Sheet"

	with open(current_csv_file, 'rb') as csvfile:
		blargh = csv.reader(csvfile)
		for row in blargh:
			name = worksheet.col_values(1)
			phone_number = worksheet.col_values(2)
			message_number = worksheet.col_values(3)
			sid = worksheet.col_values(4)
			date_sent = worksheet.col_values(5)

			column_array = [name, phone_number, message_number, sid, date_sent]
			for index, column_name in enumerate(column_array):
				row_num_of_next_empty_cell = len(column_name) + 1
				col_num = index + 1
				worksheet.update_cell(row_num_of_next_empty_cell, col_num, row[index])


def run_script():
	keep_running = True	
	message_counter = 0
	
	create_new_csv()
	while keep_running == True:
		try:
			message_counter += 1
			message_count_number = "#{}".format(message_counter)
			print message_count_number
			send_message(message_count_number, recipient_numbers)
			sleep_time = randint(60,3600)
			m, s = divmod(sleep_time, 60)
			print "-"*40
			print "     Sleeping for {} minutes ".format(m) + "and {} seconds".format(s)
			time.sleep(sleep_time)
		except:
			oauth_login()
			add_to_google_sheet()
			keep_running = False
	
parse_parameters()
run_script()







